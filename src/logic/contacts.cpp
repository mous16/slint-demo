#include "slint-demo/contacts.hpp"

#include <chrono>
#include <string>
#include <utility>

slint_demo::Contact::Contact(slint_demo::Contact::Id newId,
                             std::string newFirstName,
                             std::string newSecondName,
                             std::chrono::year_month_day newBirthday):
  id{newId}, firstName{std::move(newFirstName)}, secondName{std::move(newSecondName)}, birthday{newBirthday}
{ }

slint_demo::Contact::Contact(std::string newFirstName,
                             std::string newSecondName,
                             std::chrono::year_month_day newBirthday):
  firstName{std::move(newFirstName)}, secondName{std::move(newSecondName)}, birthday{newBirthday}
{ }

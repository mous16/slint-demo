#include "slint-demo/contact_manager.hpp"

#include <algorithm>
#include <exception>
#include <iterator>
#include <mutex>
#include <utility>
#include <vector>

#include "slint-demo/contacts.hpp"

auto slint_demo::ContactManager::findContact(Contact::Id contactId) const -> std::vector<Contact>::const_iterator
{
    const auto contactItr{
      std::find_if(storedContacts.cbegin(), storedContacts.cend(), [contactId](const auto& storedContact) {
          return contactId == *storedContact.id;
      })};

    if(contactItr == storedContacts.cend()) {
        throw std::exception{};
    }

    return contactItr;
}

auto slint_demo::ContactManager::findContact(Contact::Id contactId) -> std::vector<Contact>::iterator
{
    const auto contactItr{
      std::find_if(storedContacts.begin(), storedContacts.end(), [contactId](const auto& storedContact) {
          return contactId == *storedContact.id;
      })};

    if(contactItr == storedContacts.end()) {
        throw std::exception{};
    }

    return contactItr;
}

namespace
{
    auto prepareContacts(std::vector<slint_demo::Contact>&& contacts) -> std::vector<slint_demo::Contact>&&
    {
        for(unsigned int contactId{0U}; auto& contact: contacts) {
            contact.id = ++contactId;
        }
        return std::move(contacts);
    }
}

slint_demo::ContactManager::ContactManager(std::vector<slint_demo::Contact> contacts):
  storedContacts{prepareContacts(std::move(contacts))}
{ }

auto slint_demo::ContactManager::getContactIds() const -> std::vector<Contact::Id>
{
    const std::lock_guard lock{contactsMutex};

    std::vector<Contact::Id> ids;
    ids.reserve(storedContacts.size());
    std::transform(storedContacts.cbegin(), storedContacts.cend(), std::back_inserter(ids), [](const auto& contact) {
        return *contact.id;
    });

    return ids;
}

auto slint_demo::ContactManager::createContact(Contact& contact) -> Contact::Id
{
    if(contact.id) {
        throw std::exception{};
    }

    {
        const std::lock_guard lock{contactsMutex};

        contact.id = storedContacts.empty() ? 1U : (*storedContacts.back().id + 1U);

        storedContacts.push_back(contact);
    }

    contactsCreatedSignal(std::vector{*contact.id});
    return *contact.id;
}

auto slint_demo::ContactManager::readContact(Contact::Id contactId) const -> Contact
{
    const std::lock_guard lock{contactsMutex};

    return *findContact(contactId);
}

void slint_demo::ContactManager::updateContact(const Contact& contact)
{
    if(!contact.id) {
        throw std::exception{};
    }

    {
        const std::lock_guard lock{contactsMutex};

        Contact& storedContact{*findContact(*contact.id)};
        storedContact = contact;
    }

    contactsUpdatedSignal(std::vector{*contact.id});
}

void slint_demo::ContactManager::deleteContact(Contact::Id contactId)
{
    {
        const std::lock_guard lock{contactsMutex};

        const auto contactItr{findContact(contactId)};

        storedContacts.erase(contactItr);
    }

    contactsDeletedSignal(std::vector{contactId});
}

auto slint_demo::ContactManager::createContacts(std::vector<Contact>& contacts) -> std::vector<Contact::Id>
{
    if(std::any_of(contacts.cbegin(), contacts.cend(), [](const auto& contact) { return contact.id; })) {
        throw std::exception{};
    }

    {
        const std::lock_guard lock{contactsMutex};

        storedContacts.reserve(storedContacts.size() + contacts.size());

        for(auto& contact: contacts) {
            contact.id = storedContacts.empty() ? 0U : (*storedContacts.back().id + 1U);

            storedContacts.push_back(contact);
        }
    }

    std::vector<Contact::Id> ids;
    ids.reserve(contacts.size());
    std::transform(contacts.cbegin(), contacts.cend(), std::back_inserter(ids), [](const auto& contact) {
        return *contact.id;
    });

    contactsCreatedSignal(ids);
    return ids;
}

auto slint_demo::ContactManager::readContacts(const std::vector<Contact::Id>& contactIds) const -> std::vector<Contact>
{
    const std::lock_guard lock{contactsMutex};

    std::vector<Contact> contacts;
    contacts.reserve(contactIds.size());
    std::transform(contactIds.cbegin(), contactIds.cend(), std::back_inserter(contacts), [this](const auto& contactId) {
        return *findContact(contactId);
    });

    return contacts;
}

void slint_demo::ContactManager::updateContacts(const std::vector<Contact>& contacts)
{
    if(std::any_of(contacts.cbegin(), contacts.cend(), [](const auto& contact) { return !contact.id; })) {
        throw std::exception{};
    }

    {
        const std::lock_guard lock{contactsMutex};

        for(const auto& contact: contacts) {
            *findContact(*contact.id) = contact;
        }
    }

    std::vector<Contact::Id> ids;
    ids.reserve(contacts.size());
    std::transform(contacts.cbegin(), contacts.cend(), std::back_inserter(ids), [](const auto& contact) {
        return *contact.id;
    });

    contactsUpdatedSignal(ids);
}

void slint_demo::ContactManager::deleteContacts(const std::vector<Contact::Id>& contactIds)
{
    {
        const std::lock_guard lock{contactsMutex};

        for(const auto& contactId: contactIds) {
            const auto contactItr{findContact(contactId)};

            storedContacts.erase(contactItr);
        }
    }

    contactsDeletedSignal(contactIds);
}

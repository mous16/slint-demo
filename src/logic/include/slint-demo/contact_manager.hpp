#pragma once

#include <mutex>
#include <utility>
#include <vector>

#include <boost/signals2.hpp>
#include <boost/signals2/connection.hpp>

#include "slint-demo/contacts.hpp"

namespace slint_demo
{

    class ContactManager
    {
        mutable std::mutex contactsMutex;
        std::vector<Contact> storedContacts;

        boost::signals2::signal<void(const std::vector<Contact::Id>&)> contactsCreatedSignal;
        boost::signals2::signal<void(const std::vector<Contact::Id>&)> contactsUpdatedSignal;
        boost::signals2::signal<void(const std::vector<Contact::Id>&)> contactsDeletedSignal;

        auto findContact(Contact::Id contactId) const -> std::vector<Contact>::const_iterator;
        auto findContact(Contact::Id contactId) -> std::vector<Contact>::iterator;

      public:
        explicit ContactManager(std::vector<Contact> contacts);

        ContactManager() = default;
        ContactManager(const ContactManager&) = delete;
        ContactManager(ContactManager&&) = delete;
        ~ContactManager() = default;

        auto operator=(const ContactManager&) -> ContactManager& = delete;
        auto operator=(ContactManager&&) -> ContactManager& = delete;

        auto getContactIds() const -> std::vector<Contact::Id>;

        auto createContact(Contact& contact) -> Contact::Id;
        auto readContact(Contact::Id contactId) const -> Contact;
        void updateContact(const Contact& contact);
        void deleteContact(Contact::Id contactId);

        auto createContacts(std::vector<Contact>& contacts) -> std::vector<Contact::Id>;
        auto readContacts(const std::vector<Contact::Id>& contactIds) const -> std::vector<Contact>;
        void updateContacts(const std::vector<Contact>& contacts);
        void deleteContacts(const std::vector<Contact::Id>& contactIds);

        template<typename Slot>
        [[nodiscard]] auto registerForContactsCreated(Slot&& slot) -> boost::signals2::connection
        {
            return contactsCreatedSignal.connect(std::forward<Slot>(slot));
        }

        template<typename Slot>
        [[nodiscard]] auto registerForContactsUpdated(Slot&& slot) -> boost::signals2::connection
        {
            return contactsUpdatedSignal.connect(std::forward<Slot>(slot));
        }

        template<typename Slot>
        [[nodiscard]] auto registerForContactsDeleted(Slot&& slot) -> boost::signals2::connection
        {
            return contactsDeletedSignal.connect(std::forward<Slot>(slot));
        }
    };
}

#pragma once

#include <chrono>
#include <optional>
#include <string>

namespace slint_demo
{
    struct Contact
    {
        using Id = unsigned int;

        Contact(Id newId, std::string newFirstName, std::string newSecondName, std::chrono::year_month_day newBirthday);
        Contact(std::string newFirstName, std::string newSecondName, std::chrono::year_month_day newBirthday);
        Contact(const Contact&) = default;
        Contact(Contact&&) = default;
        ~Contact() = default;

        auto operator=(const Contact&) -> Contact& = default;
        auto operator=(Contact&&) -> Contact& = default;

        std::optional<Id> id;

        std::string firstName;
        std::string secondName;
        std::chrono::year_month_day birthday;

        auto operator<=>(const Contact&) const = default;
    };
}

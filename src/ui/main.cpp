#include <chrono>
#include <memory>
#include <vector>

#include <slint.h>
#include <slint-demo/contact_manager.hpp>
#include <slint-demo/contacts.hpp>

#include "logic_handling.hpp"

auto main([[maybe_unused]] int argc, [[maybe_unused]] char** argv) -> int
{
    using namespace std::literals::chrono_literals;

    auto contactManager{
      std::make_shared<slint_demo::ContactManager>(
        std::vector<slint_demo::Contact>{{"Francesco", "Cusolito", {1'988y, std::chrono::December, 3d}},
                                         {"Vera", "Cusolito", {2'022y, std::chrono::February, 17d}},
                                         {"Francesca", "Cappelletti", {1'988y, std::chrono::December, 16d}},
                                         {"Ciccio", "Pasticcio", {1'616y, std::chrono::April, 20d}}}
        )
    };

    auto mainWindow{slint_demo::ui::MainWindow::create()};
    {
        slint_demo::registerLogic(contactManager, mainWindow);
        mainWindow->run();
    }
    return 0;
}


#include "logic_handling.hpp"

#include <algorithm>
#include <chrono>
#include <cstddef>
#include <memory>
#include <optional>
#include <utility>
#include <vector>

#include <print>
#include <slint.h>

#include "slint-demo/contacts.hpp"

namespace
{
    auto dateToString(const std::chrono::year_month_day& date) -> std::string
    {
        return std::format("{}", date);
    }

    auto makeContactInfo(const slint_demo::Contact& contact) -> slint_demo::ui::ContactInfo
    {
        return {static_cast<int>(contact.id.value_or(0U)),
                contact.firstName.c_str(),
                contact.secondName.c_str(),
                static_cast<int>(contact.birthday.year()),
                static_cast<int>(static_cast<unsigned int>(contact.birthday.month())),
                static_cast<int>(static_cast<unsigned int>(contact.birthday.day()))};
    }

    auto makeContact(const slint_demo::ui::ContactInfo& contactInfo) -> slint_demo::Contact
    {
        if(contactInfo.id == 0) {
            return {
              contactInfo.first_name.data(),
              contactInfo.second_name.data(),
              {std::chrono::year{contactInfo.birth_year},
                                   std::chrono::month{static_cast<unsigned int>(contactInfo.birth_month)},
                                   std::chrono::day{static_cast<unsigned int>(contactInfo.birth_day)}}
            };
        }

        return {
          static_cast<slint_demo::Contact::Id>(contactInfo.id),
          contactInfo.first_name.data(),
          contactInfo.second_name.data(),
          {std::chrono::year{contactInfo.birth_year},
                               std::chrono::month{static_cast<unsigned int>(contactInfo.birth_month)},
                               std::chrono::day{static_cast<unsigned int>(contactInfo.birth_day)}}
        };
    }

    auto makeContactOrderBy(
      slint_demo::ui::ContactInfoOrderBy contactInfoOrderBy) -> slint_demo::ContactInfoSortModel::OrderBy
    {
        switch(contactInfoOrderBy) {
            using ContactInfoOrderBy = slint_demo::ui::ContactInfoOrderBy;
            using OrderBy = slint_demo::ContactInfoSortModel::OrderBy;
            case ContactInfoOrderBy::FirstName: return OrderBy::FIRST_NAME;
            case ContactInfoOrderBy::SecondName: return OrderBy::SECOND_NAME;
            case ContactInfoOrderBy::Birthday: return OrderBy::BIRTDAY;
        }
    }

    auto makeContactInfoOrderBy(
      slint_demo::ContactInfoSortModel::OrderBy contactInfoOrderBy) -> slint_demo::ui::ContactInfoOrderBy
    {
        switch(contactInfoOrderBy) {
            using OrderBy = slint_demo::ContactInfoSortModel::OrderBy;
            using ContactInfoOrderBy = slint_demo::ui::ContactInfoOrderBy;
            case OrderBy::FIRST_NAME: return ContactInfoOrderBy::FirstName;
            case OrderBy::SECOND_NAME: return ContactInfoOrderBy::SecondName;
            case OrderBy::BIRTDAY: return ContactInfoOrderBy::Birthday;
        }
    }
}

void slint_demo::ContactInfoModel::initialize(std::shared_ptr<ContactManager> contactManager)
{
    manager = std::move(contactManager);
    ids = manager->getContactIds();

    connections.clear();
    connections.emplace_back(
      manager->registerForContactsCreated([self{shared_from_this()}](const std::vector<Contact::Id>& addedIds) {
          self->ids.insert(self->ids.end(), addedIds.cbegin(), addedIds.cend());
          self->row_added(self->ids.size() - addedIds.size(), addedIds.size());
      }));
    connections.emplace_back(
      manager->registerForContactsUpdated([self{shared_from_this()}](const std::vector<Contact::Id>& updatedIds) {
          for(const auto& updatedId: updatedIds) {
              self->row_changed(static_cast<std::size_t>(std::find(self->ids.cbegin(), self->ids.cend(), updatedId) -
                                                         self->ids.cbegin()));
          }
      }));
    connections.emplace_back(
      manager->registerForContactsDeleted([self{shared_from_this()}](const std::vector<Contact::Id>& deletedIds) {
          for(const auto& deletedId: deletedIds) {
              auto idItr{std::find(self->ids.begin(), self->ids.end(), deletedId)};
              const auto index{idItr - self->ids.cbegin()};
              self->ids.erase(idItr);
              self->row_removed(static_cast<std::size_t>(index), 1);
          }
      }));

    reset();
}

auto slint_demo::ContactInfoModel::row_count() const -> std::size_t
{
    if(!manager) {
        return 0;
    }

    return ids.size();
}

auto slint_demo::ContactInfoModel::row_data(std::size_t index) const -> std::optional<ui::ContactInfo>
{
    if(!manager || index >= ids.size()) {
        return {};
    }

    return makeContactInfo(manager->readContact(ids[index]));
}

void slint_demo::ContactInfoModel::set_row_data(std::size_t index, const ui::ContactInfo& contactInfo)
{
    if(!manager || index >= ids.size() || ids[index] != static_cast<Contact::Id>(contactInfo.id)) {
        return;
    }

    manager->updateContact(makeContact(contactInfo));
}

void slint_demo::ContactInfoModel::add_row_data(ui::ContactInfo& contactInfo)
{
    if(!manager) {
        return;
    }

    auto contact{makeContact(contactInfo)};
    if(contact.id) {
        return;
    }

    manager->createContact(contact);
    contactInfo = makeContactInfo(contact);
}

void slint_demo::ContactInfoModel::remove_row_data(std::size_t index)
{
    if(!manager || index >= ids.size()) {
        return;
    }

    manager->deleteContact(ids[index]);
}

slint_demo::ContactInfoFilterModel::ContactInfoFilterModel(
  std::shared_ptr<slint::Model<ui::ContactInfo>> contactInfoModel):
  FilterModel{std::move(contactInfoModel), [this](const ui::ContactInfo& element) { return filterContacts(element); }}
{ }

auto slint_demo::ContactInfoFilterModel::filterContacts(const ui::ContactInfo& element) -> bool
{
    if(!filterText) {
        return true;
    }

    const auto contact{makeContact(element)};

    return contact.firstName.contains(*filterText) || contact.secondName.contains(*filterText) ||
           dateToString(contact.birthday).contains(*filterText);
}

auto slint_demo::ContactInfoFilterModel::getFilterText() const -> std::string
{
    return filterText.value_or(std::string{});
}

void slint_demo::ContactInfoFilterModel::setFilterText(const std::string& newFilterText)
{
    if(filterText == newFilterText || (!filterText && newFilterText.empty())) {
        return;
    }

    if(newFilterText.empty()) {
        filterText.reset();
    } else {
        filterText = newFilterText;
    }
    reset();
}

slint_demo::ContactInfoSortModel::ContactInfoSortModel(std::shared_ptr<slint::Model<ui::ContactInfo>> contactInfoModel):
  SortModel{std::move(contactInfoModel),
            [this](const ui::ContactInfo& first, const ui::ContactInfo& second) { return sortContacts(first, second); }}
{ }

auto slint_demo::ContactInfoSortModel::sortContacts(const ui::ContactInfo& first, const ui::ContactInfo& second) -> bool
{
    const auto firstContact{makeContact(!ascending ? first : second)};
    const auto secondContact{makeContact(!ascending ? second : first)};

    switch(orderBy) {
        case OrderBy::FIRST_NAME:
            return firstContact.firstName > secondContact.firstName ||
                   (firstContact.firstName == secondContact.firstName &&
                    (firstContact.secondName > secondContact.secondName ||
                     (firstContact.secondName == secondContact.secondName &&
                      firstContact.birthday > secondContact.birthday)));
        case OrderBy::SECOND_NAME:
            return firstContact.secondName > secondContact.secondName ||
                   (firstContact.secondName == secondContact.secondName &&
                    (firstContact.firstName > secondContact.firstName ||
                     (firstContact.firstName == secondContact.firstName &&
                      firstContact.birthday > secondContact.birthday)));
        case OrderBy::BIRTDAY:
            return firstContact.birthday > secondContact.birthday ||
                   (firstContact.birthday == secondContact.birthday &&
                    (firstContact.firstName > secondContact.firstName ||
                     (firstContact.firstName == secondContact.firstName &&
                      firstContact.secondName > secondContact.secondName)));
    }
}

auto slint_demo::ContactInfoSortModel::getOrderBy() const -> OrderBy
{
    return orderBy;
}

void slint_demo::ContactInfoSortModel::setOrderBy(OrderBy newOrderBy)
{
    if(orderBy == newOrderBy) {
        return;
    }

    orderBy = newOrderBy;
    reset();
}

void slint_demo::ContactInfoSortModel::setAscending(bool newAscending)
{
    if(ascending == newAscending) {
        return;
    }

    ascending = newAscending;
    reset();
}

auto slint_demo::ContactInfoSortModel::getAscending() const -> bool
{
    return ascending;
}

void slint_demo::registerLogic(std::shared_ptr<ContactManager>& contactManager,
                               slint::ComponentHandle<ui::MainWindow>& mainWindow)
{
    auto contactModel{std::make_shared<ContactInfoModel>()};
    contactModel->initialize(contactManager);

    auto contactFilterModel{std::make_shared<ContactInfoFilterModel>(contactModel)};
    auto contactFilterSortModel{std::make_shared<ContactInfoSortModel>(contactFilterModel)};

    const auto& globalContactManagerWrapper{mainWindow->global<ui::ContactManagerWrapper>()};

    globalContactManagerWrapper.set_contact_infos(contactFilterSortModel);

    globalContactManagerWrapper.set_contact_infos_filter_text(contactFilterModel->getFilterText().c_str());
    globalContactManagerWrapper.on_contact_infos_filter_changed([contactFilterModel, mainWindow]() {
        const auto& innerGlobalContactManagerWrapper{mainWindow->global<ui::ContactManagerWrapper>()};

        contactFilterModel->setFilterText(innerGlobalContactManagerWrapper.get_contact_infos_filter_text().data());
    });

    globalContactManagerWrapper.set_contact_infos_sort_order_by(
      makeContactInfoOrderBy(contactFilterSortModel->getOrderBy()));
    globalContactManagerWrapper.set_contact_infos_sort_ascending(contactFilterSortModel->getAscending());
    globalContactManagerWrapper.on_contact_infos_sort_changed([contactFilterSortModel, mainWindow]() {
        const auto& innerGlobalContactManagerWrapper{mainWindow->global<ui::ContactManagerWrapper>()};

        contactFilterSortModel->setOrderBy(
          makeContactOrderBy(innerGlobalContactManagerWrapper.get_contact_infos_sort_order_by()));
        contactFilterSortModel->setAscending(innerGlobalContactManagerWrapper.get_contact_infos_sort_ascending());
    });

    globalContactManagerWrapper.on_insert_modify_contact_info([contactManager](const ui::ContactInfo& contactInfo) {
        Contact contact{makeContact(contactInfo)};
        if(contact.id) {
            contactManager->updateContact(contact);
        } else {
            contactManager->createContact(contact);
        }
    });

    globalContactManagerWrapper.on_remove_contact_info([contactManager](const ui::ContactInfo& contactInfo) {
        contactManager->deleteContact(static_cast<Contact::Id>(contactInfo.id));
    });
}

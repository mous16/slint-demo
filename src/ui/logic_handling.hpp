#pragma once

#include <memory>
#include <optional>
#include <string>
#include <vector>

#include <boost/signals2/connection.hpp>

#include <slint.h>

#include "MainWindow.h"
#include "slint-demo/contact_manager.hpp"
#include "slint-demo/contacts.hpp"

namespace slint_demo
{
    class ContactInfoModel: public slint::Model<ui::ContactInfo>,
                            public std::enable_shared_from_this<ContactInfoModel>
    {
        std::shared_ptr<ContactManager> manager;
        std::vector<Contact::Id> ids;
        std::vector<boost::signals2::scoped_connection> connections;

      public:
        ContactInfoModel() = default;
        ContactInfoModel(const ContactInfoModel&) = delete;
        ContactInfoModel(ContactInfoModel&&) = delete;

        ~ContactInfoModel() override = default;

        auto operator=(const ContactInfoModel&) -> ContactInfoModel& = delete;
        auto operator=(ContactInfoModel&&) -> ContactInfoModel& = delete;

        void initialize(std::shared_ptr<ContactManager> contactManager);

        auto row_count() const -> std::size_t override;
        auto row_data(std::size_t index) const -> std::optional<ui::ContactInfo> override;
        void set_row_data(std::size_t index, const ui::ContactInfo& contactInfo) override;

        void add_row_data(ui::ContactInfo& contactInfo);
        void remove_row_data(std::size_t index);
    };

    class ContactInfoFilterModel: public slint::FilterModel<ui::ContactInfo>
    {
      private:
        std::optional<std::string> filterText;

        auto filterContacts(const ui::ContactInfo& element) -> bool;

      public:
        ContactInfoFilterModel(std::shared_ptr<slint::Model<ui::ContactInfo>> contactInfoModel);

        ContactInfoFilterModel() = delete;
        ContactInfoFilterModel(const ContactInfoFilterModel&) = delete;
        ContactInfoFilterModel(ContactInfoFilterModel&&) = delete;

        auto operator=(const ContactInfoFilterModel&) -> ContactInfoFilterModel& = delete;
        auto operator=(ContactInfoFilterModel&&) -> ContactInfoFilterModel& = delete;

        ~ContactInfoFilterModel() override = default;

        auto getFilterText() const -> std::string;
        void setFilterText(const std::string& newFilterText);
    };

    class ContactInfoSortModel: public slint::SortModel<ui::ContactInfo>
    {
      public:
        enum class OrderBy
        {
            FIRST_NAME,
            SECOND_NAME,
            BIRTDAY
        };

      private:
        OrderBy orderBy{OrderBy::FIRST_NAME};
        bool ascending{true};

        auto sortContacts(const ui::ContactInfo& first, const ui::ContactInfo& second) -> bool;

      public:
        ContactInfoSortModel(std::shared_ptr<slint::Model<ui::ContactInfo>> contactInfoModel);

        ContactInfoSortModel() = delete;
        ContactInfoSortModel(const ContactInfoSortModel&) = delete;
        ContactInfoSortModel(ContactInfoSortModel&&) = delete;

        auto operator=(const ContactInfoSortModel&) -> ContactInfoSortModel& = delete;
        auto operator=(ContactInfoSortModel&&) -> ContactInfoSortModel& = delete;

        ~ContactInfoSortModel() override = default;

        auto getOrderBy() const -> OrderBy;
        void setOrderBy(OrderBy newOrderBy);

        auto getAscending() const -> bool;
        void setAscending(bool newAscending);
    };

    void registerLogic(std::shared_ptr<ContactManager>& contactManager,
                       slint::ComponentHandle<ui::MainWindow>& mainWindow);
}

# Slint UI Demo
This is a small pet projects, that aims to test and showcase Slint toolkit and C++ integration.

## Build
The project depends upon Boost Signals2 and Slint. Both dependecies can be provided externally, or automatically fetched with CMake (for Slint) and Conan2 (for Boost Signals2).

The build process was tested on Windows using LLVM toolchain based on MSVC 2019, and with MSVC 2022 toolchain.  
The projects should work seamlessy also Linux with anu toolchain.

After cloning the project into `SRC_DIR`, configure and build it with CMake into `BLD_DIR`:
```sh
cmake -S "${SRC_DIR}" -B "${BLD_DIR}"
cmake --build "${BLD_DIR} -t slint_demo_ui"
```

To enable dependencies management, configure the project with those additional parameters:
```sh
cmake -S "${SRC_DIR}" -B "${BLD_DIR}" "-DSLINTDEMO_PACKAGE_MANAGER:BOOL=ON" "-DFETCHCONTENT_UPDATES_DISCONNECTED:BOOL=ON"
```

To enable developer tools, such as code analysis, configure the project with those additional paramters:
```sh
cmake -S "${SRC_DIR}" -B "${BLD_DIR}" "-DENABLE_DEVELOPER_MODE:BOOL=ON" "-DOPT_WARNINGS_AS_ERRORS:BOOL=OFF"
```

## Packaging
When project has been correctly built, it's possible to generate distribution package with CPack:
```sh
pushd "${BLD_DIR}"
cpack
popd
```

## TODO
- Fix packaging to avoid Slint generator inclusion


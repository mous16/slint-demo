if(CMAKE_VERSION VERSION_GREATER_EQUAL "3.24.0")
    cmake_policy(SET CMP0135 NEW)
endif()

include(FetchContent)
FetchContent_Declare(_project_options URL https://github.com/aminya/project_options/archive/refs/heads/main.zip)
FetchContent_MakeAvailable(_project_options)
include(${_project_options_SOURCE_DIR}/Index.cmake)
include(${_project_options_SOURCE_DIR}/src/DynamicProjectOptions.cmake)

option(SLINTDEMO_PACKAGE_MANAGER "Automatically handles dependencies fetch and setup" OFF)

if(SLINTDEMO_PACKAGE_MANAGER)
    #run_vcpkg(ENABLE_VCPKG_UPDATE)
    run_conan(
        HOST_PROFILE
        auto-cmake
        BUILD_PROFILE
        auto-cmake)
endif()

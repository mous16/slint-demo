#!/usr/bin/env bash

SRC_PATH="$(realpath "${1}")"
shift 1

cd "${SRC_PATH}" || exit 

git ls-files | grep -Ei "^.*\.(cpp|hpp|c|h)$" | xargs clang-format -i
git ls-files | grep -Ei "^(CMakeLists.txt|.*\.cmake)$" | xargs cmake-format -i
git ls-files | grep -Ei "^.*\.slint$" | xargs slint-lsp format -i

